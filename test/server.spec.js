// ./test/server.spec.js - Integration Test
const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const port = 3000;

describe("Color Code Converter API", () => {
    let server = undefined; // works without defining
    before("Start server before run tests", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening: localhost:${port}`);
            done();
        });
    });
    describe("RGB to Hex conversion", () => {

        const url = `http://localhost:${port}/rgb-to-hex?red=255&green=255&blue=255`;

        it("returns status 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

        it("returns the color in hex", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal("ffffff");
                done();
            });
        });

    });

    describe("Hex to RGB conversion", () => {

        const url = `http://localhost:${port}/hex-to-rgb?hex=ff9600`;

        it("Returns the color in RGB", (done) => {
            request(url, (error, response, body) => {
                expect(JSON.parse(body).red).to.equal("255");
                expect(JSON.parse(body).green).to.equal("150");
                expect(JSON.parse(body).blue).to.equal("0");
                done();
            });
        });

    });
    
    after("Stop server after tests", (done) => {
        server.close();
        done();
    })
});