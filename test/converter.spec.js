const expect = require('chai').expect;
const converter = require('../src/converter');

describe("Color Code Converter", () => {
  describe("RGB to Hex Conversion", () => {
    it("converts the basic colors", () => {
      const hexRed = converter.rgbToHex(255, 0, 0); // FF0000
      const hexGreen = converter.rgbToHex(0,255,0); // 00FF00
      const hexBlue = converter.rgbToHex(0,0,255);  // 0000FF

      expect(hexRed).to.equal("ff0000");
      expect(hexGreen).to.equal("00ff00");
      expect(hexBlue).to.equal("0000ff");
    });
  });

  describe("Hex to RGB Conversion", () => {
    it("Converts basic hex to RGB", () => {
      const hex = converter.hexToRgb("00ff00");

      expect(hex.red).to.equal("0");
      expect(hex.green).to.equal("255");
      expect(hex.blue).to.equal("0");
    });
  });


});