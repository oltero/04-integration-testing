/**
 * Padding outputs 2 characters allways
 * @param {string} hex one or two characters
 * @returns {string} hex code with two characters
 */
const pad = (hex) => {
  return (hex.length === 1 ? "0" + hex : hex);
}

module.exports = {
  /**
   * Converts RGB to Hex string
   * @param {number} red 0-255
   * @param {number} green 0-255
   * @param {number} blue 0-255
   * @returns {string} hex value
   */
  rgbToHex: (red, green, blue) => {
    const hexRed = red.toString(16);  // Convert to string with base 16 (hex)
    const hexGreen = green.toString(16);
    const hexBlue = blue.toString(16);
    return pad(hexRed) + pad(hexGreen) + pad(hexBlue);
  },

  /**
   * Converts Hex to RGB
   * @param {string} hex value
   * @returns {number, number, number} RGB value
   */
  hexToRgb: (hex) => {
    const red = parseInt(hex.substring(0, 2).toString(),16); // Convert to base 10
    const green = parseInt(hex.substring(2, 4).toString(),16);
    const blue = parseInt(hex.substring(4).toString(),16);

    return {"red": red.toString(), "green": green.toString(), "blue": blue.toString()};
  }


}